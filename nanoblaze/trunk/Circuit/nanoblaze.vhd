--##############################################################################
--
--  nanoblaze
--      Top view of the NanoBlaze processor
--
--      The processor is compatible with the Xilinx PicoBlaze
--      <http://www.picoblaze.info>, but different bit widths can be adapted
--      in order to have a larger processor.
--
-- falta conectar todo el display
-- conectar debounce
-- conectar cambio de bit a integer
--------------------------------------------------------------------------------
--
--  Versions / Authors
--      1.0 Francois Corthay    first implementation
--
--  Provided under GNU LGPL licence: <http://www.gnu.org/copyleft/lesser.html>
--
--  by the electronics group of "HES-SO//Valais Wallis", in Switzerland:
--  <http://www.hevs.ch/en/rad-instituts/institut-systemes-industriels/>.
--
--------------------------------------------------------------------------------
--
--  Usage
--      Set the proper values for all generics.
--
--      Edit an assembler file and compile it as the architecture of "programRom".
--
--      The "reset" signal is active high.
--
--------------------------------------------------------------------------------
--
--  Synthesis results
--      A circuit compatible with the PicoBlaze sizes gives the following
--      synthesis result on a Xilinx Spartan3-1000:
--          Number of Slice Flip Flops:           157 out of  15,360    1%
--          Number of 4 input LUTs:               446 out of  15,360    2%
--          Number of BRAMs:                        1 out of      24    4%
--
--##############################################################################

LIBRARY ieee;
  USE ieee.std_logic_1164.all;
  USE ieee.numeric_std.all;

ENTITY nanoBlaze IS
  GENERIC( 
    addressBitNb           : positive := 8;
    registerBitNb          : positive := 8;
    programCounterBitNb    : positive := 10;
    stackPointerBitNb      : positive := 5;
    registerAddressBitNb   : positive := 4;
    scratchpadAddressBitNb : natural  := 6
  );
  PORT( 
    reset       : IN     std_ulogic;
    clock       : IN     std_ulogic;
    en          : IN     std_ulogic;
	 dataAddress : OUT    unsigned(addressBitNb-1 DOWNTO 0);
    dataOut     : OUT    std_ulogic_vector(registerBitNb-1 DOWNTO 0);
    dataIn      : IN     std_ulogic_vector(registerBitNb-1 DOWNTO 0); --ver de cancelar y reemplazar por "keys"
    readStrobe  : OUT    std_uLogic;
    writeStrobe : OUT    std_uLogic;
    int         : IN     std_uLogic;
    intAck      : OUT    std_ulogic;
--	 frez_cuenta : IN     std_logic; -- Congela los displays         
--  keys        : IN     std_logic_vector(3 downto 0);	 KKKK 11110000
	 sieteSeg    : OUT    std_logic_vector(6 downto 0); -- salida display
	 muxDisplay  : Out    std_logic_vector(3 downto 0)  -- salida display
  );
END nanoBlaze ;

--==============================================================================

ARCHITECTURE struct OF nanoBlaze IS

  constant instructionBitNb: positive := 18;
  SIGNAL instruction    : std_ulogic_vector(instructionBitNb-1 DOWNTO 0);
  SIGNAL logic1         : std_ulogic;
  SIGNAL programCounter : unsigned(programCounterBitNb-1 DOWNTO 0);
  
--==============================AGREGADO TD4===================================
  SIGNAL cuenta_bits    : std_ulogic_vector(registerBitNb-1 DOWNTO 0);  -- hace bifurcacion a dataout y cuenta_showed
  SIGNAL cuenta_showed    : std_ulogic_vector(registerBitNb-1 DOWNTO 0);-- ENTRA A ENTIDAD DISPLAY 
  SIGNAL writeAux       : std_ulogic; --sirve para control de writeStrobe
  signal bcdu, bcdd, bcdc : std_logic_vector (3 downto 0);

  --==============================AGREGADO TD4===================================
COMPONENT display is
port (clk : in std_logic;
      reset : in std_logic;
      bcd0: in std_logic_vector (3 downto 0);
		bcd1: in std_logic_vector (3 downto 0);
		bcd2: in std_logic_vector (3 downto 0);
--		bcd3: in std_logic_vector (3 downto 0);
		segmento: out std_logic_vector (6 downto 0);
		enable: out std_logic_vector (3 downto 0));
end COMPONENT;

component bit2dec is
port(
 i_bit     :  in std_ulogic_vector(7 downto 0);
 o_unidad  : out std_logic_vector(3 downto 0);
 o_decena  : out std_logic_vector(3 downto 0);
 o_centena : out std_logic_vector(3 downto 0)
 );
end component;
--===============================================================================
  COMPONENT nanoProcessor
    GENERIC (
      addressBitNb           : positive := 8;
      registerBitNb          : positive := 8;
      registerAddressBitNb   : positive := 4;
      programCounterBitNb    : positive := 10;
      stackPointerBitNb      : positive := 5;
      instructionBitNb       : positive := 18;
      scratchpadAddressBitNb : natural  := 4
    );
    PORT (
      reset       : IN  std_uLogic;
      clock       : IN  std_uLogic;
      en          : IN  std_uLogic;
      progCounter : OUT unsigned(programCounterBitNb-1 DOWNTO 0);
      instruction : IN  std_ulogic_vector(instructionBitNb-1 DOWNTO 0);
      dataAddress : OUT unsigned(addressBitNb-1 DOWNTO 0);
      dataOut     : OUT std_ulogic_vector(registerBitNb-1 DOWNTO 0);
      dataIn      : IN  std_ulogic_vector(registerBitNb-1 DOWNTO 0);
      readStrobe  : OUT std_uLogic;
      writeStrobe : OUT std_uLogic;
      int         : IN  std_uLogic;
      intAck      : OUT std_ulogic
    );
  END COMPONENT;

  COMPONENT programRom
    GENERIC (
      addressBitNb : positive := 8;
      dataBitNb    : positive := 8
    );
    PORT (
      reset   : IN  std_uLogic;
      clock   : IN  std_uLogic;
      en      : IN  std_uLogic;
      address : IN  unsigned(addressBitNb-1 DOWNTO 0);
      dataOut : OUT std_ulogic_vector(dataBitNb-1 DOWNTO 0)
    );
  END COMPONENT;

BEGIN

  --==============================AGREGADO TD4===================================
  display1 : display port map
     (clk   => clock,
      reset => reset,
      bcd0  => bcdu,
		bcd1  => bcdd,
		bcd2  => bcdc,
--		bcd3: in std_logic_vector (3 downto 0);
		segmento => sieteSeg,
		enable   => muxDisplay
		);
		
		
	bcds: bit2dec port map
	 ( i_bit     => cuenta_showed,
      o_unidad  => bcdu,
      o_decena  => bcdd,
      o_centena => bcdc
    );

--===============================================================================

  logic1 <= '1';

  I_up : nanoProcessor
    GENERIC MAP (
      addressBitNb           => addressBitNb,
      registerBitNb          => registerBitNb,
      registerAddressBitNb   => registerAddressBitNb,
      programCounterBitNb    => programCounterBitNb,
      stackPointerBitNb      => stackPointerBitNb,
      instructionBitNb       => instructionBitNb,
      scratchpadAddressBitNb => scratchpadAddressBitNb
    )
    PORT MAP (
      reset       => reset,
      clock       => clock,
      en          => en,
      progCounter => programCounter,
      instruction => instruction, --este es el dataOut del programRom
      dataAddress => dataAddress,
      dataOut     => cuenta_bits, --dataOut, --dataOut de nanoProcessor
      dataIn      => dataIn,
      readStrobe  => readStrobe,
      writeStrobe => writeAux, -- va a cambiar cuenta y a writeStrobe toplevel
      int         => int,
      intAck      => intAck
    );
  
  writeStrobe <= writeAux; -- salida a top level
  
  I_rom : programRom
    GENERIC MAP (
      addressBitNb => programCounterBitNb,
      dataBitNb    => instructionBitNb
    )
    PORT MAP (
      reset   => reset,
      clock   => clock,
      en      => logic1,
      address => programCounter,
      dataOut => instruction -- dataOut de programRom es la instruccion que entra al procesador
    );

	 dataOut <= cuenta_bits; -- salida a top level siempre hace esta transferencia
	 
	 process(writeAux)
	 begin
	   if(writeAux = '1') then
		cuenta_showed <= cuenta_bits;   
		end if;
    end process;
END ARCHITECTURE struct;
