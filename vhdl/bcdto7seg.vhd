library ieee;
use ieee.std_logic_1164.all;

entity bcdto7seg is
port (reset : in std_logic;
      bcd: in std_logic_vector (3 downto 0);
		segmento: out std_logic_vector (6 downto 0));
end bcdto7seg;
--  DIAGRAMA
--		 _ _
--		| 1 |
--		6   4
--		|   |
--     -2-
--		|   |
--		7   5
--		|_3_|
-- logica negada

-- ADVERTENCIA
-- el vector esta considerado (6 a 0), pero la asignacion la hicimos mal de (0 a 6)
-- CORREGIMOS CON LA ASIGNACION DE PINES
-- segmento(6)--Pin_128
-- segmento(5)--Pin_124
-- segmento(4)--Pin_129
-- segmento(3)--Pin_121
-- segmento(2)--Pin_125
-- segmento(1)--Pin_126
-- segmento(0)--Pin_132

architecture arch of bcdto7seg is
begin

	process(reset, bcd) is 
	begin 
		if(reset = '0') then
	segmento <= "1011111";
		else
	    case bcd is              -- "1234567" -- posicion segun diagrama
         when  "0000"=> segmento <="0100000";--0
		   when  "0001"=>	segmento <="1110011";--1
		   when  "0010"=>	segmento <="0000110";--2
			when  "0011"=>	segmento <="0000011";--3
			when  "0100"=>	segmento <="1010001";--4
			when  "0101"=>	segmento <="0001001";--5
			when  "0110"=>	segmento <="0001000";--6
			when  "0111"=>	segmento <="0110011";--7
			when  "1000"=>	segmento <="0000000";--8
			when  "1001"=>	segmento <="0010001";--9
			when others => segmento <="1011101";-- salida de error NO DECIMAL
		 end case;	
	   end if;
	end process; 
end arch;
