library ieee;
use ieee.std_logic_1164.all;

entity multiplexor is
port (clk:in std_logic; -- toma el clock de clk2
      reset : in std_logic;
      D0: in std_logic_vector (3 downto 0);
		D1: in std_logic_vector (3 downto 0);
		D2: in std_logic_vector (3 downto 0);
--		D3: in std_logic_vector (3 downto 0);
		salida: out std_logic_vector (3 downto 0);
		MUX: out std_logic_vector (3 downto 0));
end multiplexor;


architecture Behavioral of multiplexor is
type estados is (rst, v0, v1, v2, v3);
signal estado : estados;
begin

    visualizadores: process (reset, clk) begin
        if (reset = '0') then -- entra en estado de reset
            estado <= rst;
            MUX <= x"F"; -- apaga todos los display
            salida <= "1111"; -- pone el bcd en salida de ERROR (no decimal en bcdto7seg)
        elsif rising_edge(clk) then
            case estado is
                when v0 =>
                    salida <= D0; -- unidad
                    MUX <= "1110";-- display de la derecha
                    estado <= v1;
                when v1 =>
                    salida <= D1;
                    MUX <= "1101";
                    estado <= v2;
                when v2 =>
                    salida <= D2;
                    MUX <= "1011";
                    estado <= v3;
                when v3 =>
                   -- salida <= D3;-- no será utilizado
                    MUX <= "1111"; -- 1111 d3 no se muestra
                    estado <= v0;
					 when others =>    -- salida del reset
					     salida <= D0;
                    MUX <= "1110";
                    estado <= v1;
            end case;
        end if;
    end process;
end Behavioral;