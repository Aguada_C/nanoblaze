                ;===============================================================
                ; nanoTest.asm
                ; 1- Cambiar la entrada FC por acceso a memoria
                ; 2- Agregar el stop cuenta
                ; 3- Lachear el dato a unos 50 hz
                ; 4- leer el dato de salida con readstrobe en 1
                ; 5- Agregar opcion de error
                ;===============================================================
                ; 1) Test logical operations with direct values
                ;---------------------------------------------------------------
						                LOAD      s7, 10 -- VER QUE ES FC
                                                                LOAD      s1, 00 -- LOOP
                                                                LOAD      s2, 00 -- LOOP 2
                                                                LOAD      s3, 00 -- LOOP 3
                                                                LOAD      s0, 00 -- CUENTA readStrobe en 1
                                                                OUTPUT    s0, FD
                                                                COMPARE   s1, FF -- 006     
                                                                JUMP      Z,  00A
                                                                ADD       s1, 01  
                                                                JUMP      NZ, 006
                                                                COMPARE   s2, FF --00A    
                                                                JUMP      Z,  00E
                                                                ADD       s2, 01  
                                                                JUMP      NZ, 00A
                                                                COMPARE   s3, 03 --00E     
                                                                JUMP      Z,  012
                                                                ADD       s3, 01  
                                                                JUMP      NZ, 00E
                                                                LOAD      s1, 00 --012
                                                                LOAD      s2, 00
                                                                LOAD      s3, 00
                                                                ADD       s0, 01
                                                                OUTPUT    s0, FD
                                                                COMPARE   s0, s7 --CONTROL 
                                                                JUMP      Z,  01A --18
                                                                JUMP      NZ, 006 
                                                                LOAD      s0, 01
                                                                OUTPUT    s0, 00
                                                                JUMP      endOfMemory
   


ADDRESS 3FD
         error: LOAD      s0, 00
                OUTPUT    s0, 00
                ;===============================================================
                ; End of instruction memory
                ;---------------------------------------------------------------
   endOfMemory: JUMP      endOfMemory
