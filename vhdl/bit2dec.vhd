-- por la configuracion que le voy a dar, el numero llega hasta 240 "11110000"

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity bit2dec is
port(
 i_bit     :  in std_ulogic_vector(7 downto 0);
 o_unidad  : out std_logic_vector(3 downto 0);
 o_decena  : out std_logic_vector(3 downto 0);
 o_centena : out std_logic_vector(3 downto 0)
 );
end bit2dec;

architecture arch of bit2dec is

signal aux, centena, decena, unidad : integer;


begin

aux  <= conv_integer(unsigned(i_bit));

process (aux)
begin
  centena <= aux / 100;
  decena  <= (aux rem 100) / 10;
  unidad  <= (aux rem 10);
  
end process;

o_unidad  <= conv_std_logic_vector(unidad,  4);
o_decena  <= conv_std_logic_vector(decena,  4);
o_centena <= conv_std_logic_vector(centena, 4);

end arch;