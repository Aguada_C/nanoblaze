-- CLOCK PARA LA DETERMINACION DE BARRIDO
-- Si deseo que el barrido sea de 1KHz, para los 4 display sera de 250
--50MHZ/1KHZ = 50000
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity clk2 is
    Port (
        clk: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end clk2;

architecture arch of clk2 is
signal contador: integer range 0 to 50000 := 0;
signal sal: std_logic;
begin

	process(reset, clk) is 
		-- Declaration(s) 
	begin 
		if(reset = '0') then
			sal <= '0';
		elsif(rising_edge(clk)) then
			if (contador < 25000) then
			sal <= '1';
			contador<= contador + 1;
			elsif (contador >= 25000 and contador <50000) then
			sal <= '0';
			contador<= contador +1;
			elsif (contador >= 50000) then
			sal <= '0';
			contador <=0;
			else
		   end if;
		end if;
	end process; 
	salida <= sal;
end arch;
