--bloque geneal encargado de mostrar los datos
--tomal las cuatro salida del encoder en bcd y la muestra en los displays
library ieee;
use ieee.std_logic_1164.all;

entity display is
port (clk : in std_logic;
      reset : in std_logic;
      bcd0: in std_logic_vector (3 downto 0);
		bcd1: in std_logic_vector (3 downto 0);
		bcd2: in std_logic_vector (3 downto 0);
--		bcd3: in std_logic_vector (3 downto 0);
		segmento: out std_logic_vector (6 downto 0);
		enable: out std_logic_vector (3 downto 0));
end display;

architecture arch of display is

component clk2 is
    Port (
        clk: in  STD_LOGIC;
        reset  : in  STD_LOGIC;
        salida : out STD_LOGIC
    );
end component;

component multiplexor is
port (clk:in std_logic;
      reset : in std_logic;
      D0: in std_logic_vector (3 downto 0);
		D1: in std_logic_vector (3 downto 0);
		D2: in std_logic_vector (3 downto 0);
		--D3: in std_logic_vector (3 downto 0);
		salida: out std_logic_vector (3 downto 0);
		MUX: out std_logic_vector (3 downto 0));
end component;

component bcdto7seg is
port (reset : in std_logic;
      bcd: in std_logic_vector (3 downto 0);
		segmento: out std_logic_vector (6 downto 0));
end component;

signal clk250 : std_logic;
signal aux: std_logic_vector (3 downto 0);
begin

clk1 : clk2 port map
(clk => clk,
reset=> reset,
salida =>clk250);

multiplexor1 : multiplexor port map
(clk => clk250,
reset => reset,
D0 =>bcd0,
D1 =>bcd1,
D2 =>bcd2,
--D3 =>bcd3,
salida => aux, -- bcd sincronizada con el display correspondiente
MUX => enable);

siete_seg : bcdto7seg port map
(reset => reset,
bcd => aux, -- bcd
segmento => segmento); -- transformacion a 7 segm

end arch;