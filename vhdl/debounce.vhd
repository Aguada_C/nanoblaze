-- Antirebote
library ieee;
use ieee.std_logic_1164.all;

entity debounce is
port(
 i_reset  :  in std_logic;
 i_clk    :  in std_logic;
 i_pulsar :  in std_logic;
 o_pulsar : out std_logic
 );
 end debounce;
 
 architecture arch of debounce is
 
 signal q1, q2, q3 : std_logic;

 begin
 
 process (i_clk)
 begin
 if (rising_edge (i_clk)) then
     if i_reset = '0' then
	      q1 <= '0';
	      q2 <= '0';
	      q3 <= '0';
     else
         q1 <= i_pulsar;
	      q2 <= q1;
	      q3 <= q2;
     end if;
 end if;
 end process;
 o_pulsar <= q1 and q2 and (not q3);
 end arch;